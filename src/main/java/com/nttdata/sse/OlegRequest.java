package com.nttdata.sse;



import org.apache.jmeter.config.Arguments;
import org.apache.jmeter.protocol.java.sampler.AbstractJavaSamplerClient;
import org.apache.jmeter.protocol.java.sampler.JavaSamplerContext;
import org.apache.jmeter.samplers.SampleResult;

public class OlegRequest extends AbstractJavaSamplerClient {

	@Override
	public Arguments getDefaultParameters() {
		Arguments arguments = new Arguments(); 
		arguments.addArgument("responseValue", "${testResponse}");
		return arguments ;
	}
	@Override
	public SampleResult runTest(JavaSamplerContext context) {
		SampleResult result = new SampleResult();

        boolean success = true;

        result.sampleStart();

        // Write your test code here.
        getNewLogger().info("I am in the JavaRequest");
        getNewLogger().info("The parameter: ["+ context.getParameter("responseValue", "not found")+"]");
        //


        result.sampleEnd();

        result.setSuccessful(success);

        return result;
	}

}
