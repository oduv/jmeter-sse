package com.nttdata.sse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JmeterSseApplication {

	public static void main(String[] args) {
		SpringApplication.run(JmeterSseApplication.class, args);
	}

}
